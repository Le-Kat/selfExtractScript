// supress stylistic warnings
#![allow (unused_parens)]
//#![allow()]

use std::fs::File;
use std::io::{Error, Write};

use base64::engine::general_purpose;
use base64::write::EncoderStringWriter as B64Encoder;
use clap::Parser;
use flate2::write::GzEncoder;
use flate2::Compression;
use tar::Builder;

#[derive (Parser, Debug)]
#[command (author, version, about, long_about = None)]
struct Args {
	
	#[arg (short, long, num_args (1..))]
	files: Vec <String>,
	
	#[arg (short, long, default_value_t = String::from ("-"))]
	output: String,
	
	// TODO: handle file exclusions
	//#[arg (short, ling, num_args (0..))]
	//exclude: Vec <String>,
	
	#[arg (short, long, default_value_t = cfg! (debug_assertions))]
	verbose: bool,
}

fn main() -> Result <(), Error> {
	
	let args = Args::parse();
	if (cfg! (debug_assertions) && args.verbose) {
		
		eprintln! ("{:?}\n", args);
	}
	
	let tgz_archive: Vec<u8> = tgz_archive (args.files, args.verbose)?;
	let b64_data: String = b64_data (tgz_archive)?;
	file_generator (b64_data, args.output)?;
	
	Ok (())
}

fn tgz_archive (file_list: Vec <String>, verbose_output: bool) -> Result <Vec <u8>, Error> {
	
	let gz_encoder = GzEncoder::new (Vec::new(), Compression::default());
	let mut builder = Builder::new (gz_encoder);
	
	for file in file_list {
		
		let file_path = std::path::Path::new (&file);
		if (file_path.is_dir()) {
			
			match builder.append_dir_all (file_path, &file) {
				
				Ok (val) => {
					
					if (verbose_output) {
						
						eprintln! ("{}", file);
					}
					val
				}
				Err (err) => return Err (err),
			};
			
			continue;
		}
		
		let mut input_file = File::open (&file)?;
		match builder.append_file (file_path.file_name().unwrap(), &mut input_file) {
			
			Ok (val) => {
				
				if (verbose_output) {
					
					eprintln! ("{}", file);
				}
				val
			}
			Err (err) => return Err (err),
		};
	}
	
	if (verbose_output) {
		
		eprintln! ("");
	}
	
	let tgz_archive = builder.into_inner()?.finish()?;
	
	Ok (tgz_archive)
}

fn b64_data (data: Vec <u8>) -> Result <String, Error> {
	
	let mut b64_encoder = B64Encoder::new (&general_purpose::STANDARD);
	b64_encoder.write_all (&data)?;
	
	Ok (b64_encoder.into_inner())
}

fn file_generator (b64_data: String, output_file: String) -> Result <(), Error> {
	
	let file_string: String = format! (
		
		"#!/bin/sh\necho \"{}\" | base64 -d | tar xzvf -",
		b64_data
	);
	
	if (output_file.eq ("-")) {
		
		println! ("{}", file_string);
	} else {
		
		std::fs::write (output_file, file_string)?;
	}
	
	Ok(())
}
